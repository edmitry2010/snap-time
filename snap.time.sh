#!/bin/bash

namein=$(echo $0 | awk -F'/' '{print $NF}')
perin=($*)

dir_mount='/.snap'	# Папочка для монтирования дисков.
dir_snap='dmitry.snap'	# Папочка для снапшотов, на каждом диске, будет своя такая.

# Определяется сам, в функции StartParam
#dir_conf='/etc/myserver/scrypts/snap-time'

# Массив с бинарниками которые потребуются в скрипте для корректной работы в crontab
ArrSearchBin=( systemctl umount mount btrfs sleep mkdir xargs date find grep awk cat mv df ls wc )

##### BIN beginning #####
bin_systemctl='/usr/bin/systemctl'
bin_umount='/usr/bin/umount'
bin_mount='/usr/bin/mount'
bin_btrfs='/usr/sbin/btrfs'
bin_sleep='/usr/bin/sleep'
bin_mkdir='/usr/bin/mkdir'
bin_xargs='/usr/bin/xargs'
bin_date='/usr/bin/date'
bin_find='/usr/bin/find'
bin_grep='/usr/bin/grep'
bin_awk='/usr/bin/awk'
bin_cat='/usr/bin/cat'
bin_mv='/usr/bin/mv'
bin_df='/usr/bin/df'
bin_ls='/usr/bin/ls'
bin_wc='/usr/bin/wc'

pause='0.5'	# Пауза, в некоторых местах, указана пауза, она не критична но пусть лучше будет.

function StartParam() {
  FileStart=$0
  NameFileStart=$(echo $0 | awk -F'/' '{print $NF}')
  inDirConf=$(echo $FileStart | sed "s|/$NameFileStart||g")

  if ! [ -f $namein ]; then
    namein=$namein.sh
  fi

  if [[ $inDirConf == '.' ]]; then
    dir_conf=`pwd`
  else
    dir_conf=$inDirConf
  fi

  if [ -e /usr/bin/snap.time ]; then
    dir_conf=$(ls -al /usr/bin/snap.time | sed 's|/snap.time.sh||g' | awk '{print $NF}')
  fi

  if [ -f $dir_conf/lib/color.table ]; then
    . $dir_conf/lib/color.table
    ColorView='yes'
  else
    ColorView='no'
  fi

  if [ -f $dir_conf/lib/all.func ]; then
    . $dir_conf/lib/all.func
  else
    echo 'Не найден файл с функциями, выходим'
    exit 0
  fi
}
StartParam

if [[ $perin == init ]]; then
  PrintDisplay "$ViewYellow Предварительная настройка"
  StartInit
else
  TestBIN
fi

PrintDisplay "$ViewYellow Проверка наличие файла: $ViewGreen$dir_conf/snap.uuid"
if [ -f $dir_conf/snap.uuid ]; then
  . $dir_conf/snap.uuid
else
  SetingHelp
  SetupUuidFile
  StartInit
  exit 0
fi

TestDir $dir_mount

case $perin in
  snapc) PrintDisplay "$ViewYellow Создаём снапшоты"
	 MountDisk umount $dir_mount
	 SnapCreate;;
  snapr) PrintDisplay "$ViewYellow Восстанавливаем снапшоты"
	 read -r -p 'Если у вас работает какой-то сайт или что-то с базой данных, будет лучше остановить эту работу, продолжаем? (Y|y): ' ReadPerIN
	 case $ReadPerIN in
	   Y|y|Yes|yes) SelectDisk
			MountDisk mount $DiskSelekt
			SnapRestore $DiskSelekt
			MountDisk umount $DiskSelekt;;
	 esac
	 unset ReadPerIN
	 unset DiskSelekt;;
  mount) PrintDisplay "$ViewYellow Подключаем диск"
	 MountDisk umount $dir_mount
	 SelectDisk
	 MountDisk mount $DiskSelekt
	 unset DiskSelekt;;
 umount) PrintDisplay "$ViewYellow Отключаем диск"
	 MountDisk umount $dir_mount;;
   show) PrintDisplay "$ViewYellow Смотрим снапшоты"
	 ViewSnaps;;
    del) PrintDisplay "$ViewYellow Удаление снапшотов"
	 MountDisk umount $dir_mount
         SelectDisk
	 MountDisk mount $DiskSelekt
	 DeleteSnap $DiskSelekt
	 MountDisk umount $dir_mount
	 unset DiskSelekt;;
      *) PrintDisplay "$ViewYellow Какую-то фигню указал, надо указывать что нибудь из этого:"
	 echo
	 if [[ -e /etc/systemd/system/snap-time.service && -e /etc/systemd/system/snap-time.timer ]]; then
	   PrintDisplay "$ViewYellow Посмотреть журнал выполнения таймера:$ViewGreen journalctl -eu snap-time"
	 fi
	 echo
	 PrintDisplay "$ViewGreen snap.time init	$ViewYellow- Первичная инициализация, проверяет все бинарники."
	 PrintDisplay "$ViewGreen snap.time snapc	$ViewYellow- Создаёт снапшоты по указанным дискам и subvolume, именно с этим параметром надо будет прописать в crontab."
	 PrintDisplay "$ViewGreen snap.time snapr	$ViewYellow- Восстановить снапшот. Спросит какой диск и номер снапшота, после восстановления будет автоматический ребут."
	 PrintDisplay "$ViewGreen snap.time mount	$ViewYellow- Подмонтировать диск. Спросит какой диск и подмонтирует в указанную папочку, по умолчанию /.snap"
	 PrintDisplay "$ViewGreen snap.time umount	$ViewYellow- Отмонтировать диск. Спрашивать не будет, просто отмонтирует если подмонтирован любой из дисков."
	 PrintDisplay "$ViewGreen snap.time show	$ViewYellow- Покажет все subvolume и снапшоты, на всех прописанных в snap.uuid дисках."
	 PrintDisplay "$ViewGreen snap.time del		$ViewYellow- Удаление снапшотов";;
esac
